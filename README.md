OptiLink
.NET C# library:
- connect to OPtiRE and Optilayer via COM
- COM interop classes are attached and OptiRE/Optilayer need to be installed and accessible
- opens a COM link with OptiRE or Optilayer
- handles data transfer of spectral data for fitting and returns results
- runs thickness and extended fit results on OptiRE
---
requires:
* installed OptiRE and/or Optilayer: details see (https://www.optilayer.com/)
* COM Reference doptire -> OptiRE needs to be installed
* COM Reference Dopti -> Optilayer needs to be installed on target (optional)
