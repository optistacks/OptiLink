﻿/* ====================================================================
 *  Optilayer interface class
 *  library to transfer spectral data as target to OPtilayer
 *
 * Author: Anton Dietrich
 * Date: 31.01.2014
 *
 * Copyright (C) 2014-2022 Anton Dietrich
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  ======================================================================
 */
using System.Collections;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;
using System.Runtime.Versioning;

namespace OptiLink;

/// <summary>
/// COM interface to Optilayer: access functions
/// </summary>
[SupportedOSPlatform("Windows7.0")]
public static class OptiLayer
{
    private static Dopti.OptiLayer_Application olayer;
    public static bool Connected { get; set; } = false; //!< connection established 
    private static string pdir = "NA";
    private static string rootDir = "NA";
    private static Process OLprocess;

    /// <summary>
    /// initialize COM connection to Optilayer
    /// </summary>
    /// <returns>success</returns>
    public static bool OL_Initialize()
    {
        if (olayer == null)
        {
            // verify if Optilayer is open and running
            bool isopen = ORE_IF.IsRunning("dopti");
            if (!isopen)
            {
                // launch the application to allow connection and keep it open after closing the COM object
                var orexe = @"c:\Program Files\OptiLayer\dopti.exe";
                if (!File.Exists(orexe))
                {
                    // try the 32 bit version
                    orexe = @"c:\Program Files (x86)\OptiLayer\dopti.exe";
                }
                var sinfo = new ProcessStartInfo(orexe)
                {
                    UseShellExecute = false
                };
                try
                {
                    // launch Optilayer
                    OLprocess = Process.Start(sinfo);
                    System.Threading.Thread.Sleep(1000); // wait a bit before proceeding to connect with it
                }
                catch (Exception e)
                {
                    ORE_IF.OptiMessageSend("can not launch Optilayer:" + e.Message + " Try to open Optilayer manually:");
                    return false;
                }
            }
            // Link with Optilayer using COM
            try
            {
                //olayer = new Dopti.OptiLayer_ApplicationClass();
                olayer = new Dopti.OptiLayer_Application();
            }
            catch
            {
                ORE_IF.OptiMessageSend(" Can't open Optilayer: Optilayer COM link");
                Connected = false;
                return false;  // exit if not started
            }
        }
        if (olayer == null)
        {
            return false;
        }
        else
        {
            Connected = true;
            //				olayer.LoadProblemConfigurationFiles();
            rootDir = olayer.RootDirectory; // get the current root and problem directory
            pdir = ProblemDir;
            return true;
        }
    }


    public static string OLroot => olayer == null? string.Empty: olayer.RootDirectory; //!< active root directory in Optilayer

    public static string ProblemDir //!< active problem directory in Optilayer
    {
        set
        {
            if (value == pdir) return;  // do not re-load if this is already set
            List<string> pda = ORE_IF.GetPDirs(olayer!.RootDirectory);  // get actual list of problem directories
            if (pda.Contains(value))     // this is to avoid creating new empty problem directory in case user has changed it
            {
                olayer.ProblemDirectory = value;
                olayer.LoadProblemConfigurationFiles();
                pdir = olayer.ProblemDirectory;
            }
            else
            {
                ORE_IF.OptiMessageSend("Optilayer problem directory " + value + " not found ?");
            }

        }
        get
        {
            return olayer == null? String.Empty: olayer.ProblemDirectory;
        }
    }

    

    /// <summary>
    /// Send a measurement page of spectral data to Optilayer as a target
    /// </summary>
    /// <param name="points">number of measurement points</param>
    /// <param name="angle">incidence angle </param>
    /// <param name="Mtype">measurement type T/RA/BRA</param>
    /// <param name="wvl">wavelength array</param>
    /// <param name="Intensity">intensity array</param>
    /// <param name="Tolernace">tolerance array</param>
    /// <param name="append">appen to current page ?</param>
    /// <param name="name">title for measurements</param>
    /// <param name="comment">comment string</param>
    /// <returns></returns>
    public static bool SendTargetSpectrum(int points, double angle, string Mtype, Array wvl, Array Intensity, Array Tolernace, bool append, string name, string comment)
    {
        List<string> qual = new List<string>();
        for (int i = 0; i < wvl.Length; i++)
        {
            qual.Add("");
        }
        //ORE_IF.GetMeasType(spdata.MType); // may need to be called
        Array Qual = qual.ToArray();
        bool isOK = olayer!.Databases.TargetDatabase.LoadTargetPage(points, angle, Mtype, wvl, Intensity, Tolernace, Qual, append, name, comment);
        return isOK;
    }

   
    private static void OL_error()
    {
        // get last ORE Error and display a message:
        string Oerr = olayer == null? "unknown": olayer.LastErrorText;
        ORE_IF.OptiMessageSend("Optilayer Error: " + Oerr);
    }

    public static void closeConnection()    //!< close optilayer COM connection
    {
        if (olayer != null)
        {
            Marshal.ReleaseComObject(olayer);
        }
        olayer = null;
        Connected = false;
    }

}
