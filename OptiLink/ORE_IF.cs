﻿/* ====================================================================
 *  OptiRE interface class
 *  library to transfer data and commands to OptiRE via COM
 *
 * Author: Anton Dietrich
 *
 * Copyright (C) 2008-2022 Anton Dietrich
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  ======================================================================
 */


using doptire;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Runtime.Versioning;
using System.Text;

namespace OptiLink;

/// <summary>
/// string constants used for GetChar() characteristics in OptiRe
/// </summary>
public static class ORECharNames
{
    public const string Ta = "TA";
    public const string Ra = "RA";
    public const string BRa = "BRA";
    public const string Psi = "Psi";
    public const string Delta = "Delta";
}


/// <summary>
/// class to start a COM connection with OptiRE
/// 
/// </summary>
[SupportedOSPlatform("Windows7.0")]
public static class ORE_IF
{
    // interface to connect to OptiRe
    private static OptiRE_Application ORE;
    private static Process OREprocess = null;
    public static bool Connected { get; private set; } = false;
    public static bool OREShow { get; set; } = true; //!< make ORE windows visible
    public static int OreDelay { get; set; } = 5; // delay in 5 ms intervals to wait for results 
    // remember last measurement set sent to ORE
    private static string LastMeasSet = "NA";
    const string RequiredVersion = "9.51;9.96;10.48;11.03;11.65;12.12;12.83;13.77;14.57;15.12;15.88"; //!< OptiRE versions supported and tested
    private static bool reMinimize = true;  // set a flag after a problem directory change to minimze window
    private static bool IsSingleFile { get; set; } = false;
    public static string OptiVersion { get; private set; } = "unknown";   //!< holds the current version  info
    public static string Problem { get; private set; } = "NA";  //!< selected problem directory 
    public static string RootDir { get; private set; } = "NA";  //!< selected root directory
    public enum OptiCorrections //!< OptiRE measurement correction type
    {
        ctNoCorr = 0,   //!< no correction
        ctConst = 1,    //!< constant correction
        ctLinear = 2,   //!< linear correction
        ctQuadratic = 3 //!< quadratic correction
    }
    /// <summary>
    /// event raised on OptiRE error messages
    /// </summary>
    public static event EventHandler<string> OptiMessage;

    public static void OptiMessageSend(string msg)
    {
        OptiMessage?.Invoke(sender: "OptiRE:", msg);
    }


    // structures to handle minimize parent window of OptiRE
    [DllImport("user32.dll")]
    [return: MarshalAs(UnmanagedType.Bool)]
    static extern bool GetWindowPlacement(IntPtr hWnd, ref WINDOWPLACEMENT lpwndpl);

    [DllImport("user32.dll")]
    static extern bool SetWindowPlacement(IntPtr hWnd,
                                          ref WINDOWPLACEMENT lpwndpl);
    private struct POINTAPI
    {
        public int x;
        public int y;
    }

    private struct RECT
    {
        public int left;
        public int top;
        public int right;
        public int bottom;
    }

    private struct WINDOWPLACEMENT      // used to minimize existing window
    {
        public int length;
        public int flags;
        public int showCmd;
        public POINTAPI ptMinPosition;
        public POINTAPI ptMaxPosition;
        public RECT rcNormalPosition;
    }

    // Declare the events which will be generated.
    public static event System.EventHandler OREDirCHangeEvent;  //!< OptiRE problem directory change event
    public static event System.EventHandler ResultsReadyEvent;  //!< ResultsReady event for fitting processes

    private static void RaiseResultsReadyEvent()
    {
        ResultsReadyEvent?.Invoke(ORE, new EventArgs());
    }

    private static void ORE_OnFinishComputations()
    {
       // RaiseResultsReadyEvent();
    }

    private static void RaiseDirChangeEvent()
    {
        if (OREDirCHangeEvent != null)
        {
            OREDirCHangeEvent(ORE, new EventArgs());
            reMinimize = true;
        }
    }

    internal static bool IsRunning(string app)  //!< check if OptiRE is already running
    {
        var IsL = Process.GetProcessesByName(app);
        var isOPen = IsL.Length > 0;
        return isOPen;
    }

    /// <summary>
    /// close the OPtiRE COM link
    /// </summary>
    public static void UnloadOptiRE()
    {

        if (ORE != null)
        {
            ORE.OnFinishComputations -= ORE_OnFinishComputations;
            ORE.OnModelChanged -= ORE_OnModelChanged;
            Marshal.ReleaseComObject(ORE);
        }
        ORE = null;
        //  Clear garbage collection, otherwise OptiRE will remain loaded
        GC.Collect();
    }

    /// <summary>
    /// try to link with OptiRE via COM
    /// </summary>
    /// <returns></returns>
    public static bool ORE_Initialize()
    {
        if (ORE == null)
        {
            Connected = false;  // set the not connected flag
            //OptiMessageSend("Try to link with OptiRE ...:");
            // verify if OptiRE is open and running
            bool isopen = ORE_IF.IsRunning("doptire");
            if (!isopen)
            {
                // launch the application to preserve last used root directory and keep it open after closing SA
                var orexe = @"c:\Program Files\OptiLayer\doptire.exe";
                if (!File.Exists(orexe))
                {
                    // try the 32 bit version
                    orexe = @"c:\Program Files (x86)\OptiLayer\doptire.exe";
                }
                var sinfo = new ProcessStartInfo(orexe);
                //var sinfo = new ProcessStartInfo("doptire");
                sinfo.UseShellExecute = false;
                try
                {
                    // launch OptiRE
                    OREprocess = Process.Start(sinfo);
                    Task tsk = Task.Delay(2500);
                    tsk.Wait();
                    //System.Threading.Thread.Sleep(5000); // wait a bit before proceeding to connect with it
                }
                catch (Exception e)
                {
                    OptiMessageSend("Please open OptiRE manually:can't launch OptiRE:" + e.Message);
                    return false;
                }
            }
            // try to create a COM link with OptiRE
            try
            {
                //ORE = new doptire.OptiRE_ApplicationClass();
                ORE = new doptire.OptiRE_Application();
            }
            catch
            {
                OptiMessageSend("Can't open OptiRe: OptiRE COM link");
                return false;  // exit if not started
            }
        }
        if (ORE == null)
        {
            return false;
        }
        // check licence key
        var key = ORE.KeyID;
        if (key == 0)
        {
            OptiMessageSend("Hardware key problem detected, please, start OptiRE separately");
            return false;
        }
        // check Optilayer version
        try
        {
            OptiVersion = ORE.Version;
        }
        catch
        {
            OptiVersion = "unknown";      // unknown
        }
        string[] suppVersions = RequiredVersion.Split(';');
        bool supported = false;
        for (int i = 0; i < suppVersions.Length; i++) supported |= OptiVersion.Contains(suppVersions[i].Trim());
        if (!supported)
        {
            OptiMessageSend("Installed OL Version: " + OptiVersion + " is not compatible with this version of SA");
        }
        // only 13.77+ has SingleFileMode: all others still use problem directories
        if (IsSingleFile)
        {
            OptiMessageSend("Single File mode is not supported by SA! Please use Problem Directory mode!");
            Connected = false;
            ORE_IF.UnloadOptiRE();
            return false;
        }
        else
        {
            // try to verify the problem directory "SL-Match" exists
            string pd = Path.Combine(ORE.RootDirectory, "SL-Match");
            //if the SL-Match directory is not there it may be not configured correctly
            if (!Directory.Exists(pd))
            {
                string error_msg = " Can not find problem directory \"SL-Match\" in current Root Directory:" + ORE.RootDirectory;
                OptiMessageSend(error_msg);
                Connected = false;
                return false;  // exit if not started
            }
            Problem = ORE.ProblemDirectory;
            RootDir = ORE.RootDirectory;
        }
        ORE.Visible = OREShow; // male it invisible in the background
        ORE.FittingWindow.Silent = true;
        // Length units (nm)
        ORE.Data.U_Length = doptire.LengthUnits.L_NM;
        // Spectral units the same as Length units
        ORE.Data.U_Spectral = doptire.SpectralUnits.Sp_Length;
        // Physical thicknesses
        ORE.Data.U_Thickness = doptire.ThicknessUnits.TH_PHYS;
        ShowWindow(2);    // minimize the OptiRE windows
        Connected = true; 
        if (reMinimize)
        {
            MinimizeOptiRE();
        }
        ORE.OnFinishComputations += new IOptiRE_ApplicationEvents_OnFinishComputationsEventHandler(ORE_OnFinishComputations);
        ORE.OnModelChanged += ORE_OnModelChanged;
        return true;
    }

    private static void ORE_OnModelChanged(ModelChangeReason Reason)
    {
        if (Reason == ModelChangeReason.mod_RE_Finished || Reason == ModelChangeReason.mod_RE_Terminated)
        {
            RaiseResultsReadyEvent();
        }
    }

    /// <summary>
    /// send OptiRE to background and minimize windows
    /// after a dir change and a fit the fitting window may show up
    /// </summary>
    static void MinimizeOptiRE()
    {
        ShowWindow(1); // show the full ORE window and then minimize
        System.Threading.Thread.Sleep(500); // wait at least 500ms
        reMinimize = false;
        ShowWindow(2); // minimize
    }

    public static bool REisReady => ORE.Data.IsDataReadyForRE;

    /// <summary>
    /// hide the ORE windows
    /// </summary>
    public static void HideWindows() { ShowWindow(2);  }

    /// <summary>
    /// load the last  measurement entry from the databse list to memory
    /// </summary>
    /// <returns></returns>
    static string LoadLastMeasurement()
    {
        int nd = ORE.Databases.MeasurementDatabase.FileListCount;
        if (nd == 0) return "N/A";
        string[] ll = new string[nd];
        object pp = (Array)ll;
        bool success = ORE.Databases.MeasurementDatabase.FileList(ref nd, ref pp);
        ll = (string[])pp;
        ORE.Databases.MeasurementDatabase.Load(ll[nd - 1]);
        return ll[nd - 1];
    }


    /// <summary>
    /// get all problem directories under given root
    /// </summary>
    /// <param name="rt">root directory</param>
    /// <returns>list of subdirectories</returns>
    public static List<string> GetPDirs(string rt)
    {
        List<string> pd = new ();

        DirectoryInfo MyRoot = new DirectoryInfo(rt);
        DirectoryInfo[] MySub = MyRoot.GetDirectories();
        foreach (DirectoryInfo D in MySub)
        {
            pd.Add(D.Name);
        }
        pd.Sort();
        return pd;
    }

    /// <summary>
    /// set or get active problem directory
    /// </summary>
    public static string ProbDir
    {
        set
        {
            if (value == Problem) return;  // do not re-load if this is already set
            List<string> pda = GetPDirs(ORE.RootDirectory);  // get actual list of problem directories
            if (pda.Contains(value))     // this is to avoid creating new empty problem directory in case user has changed it
            {

                ShowWindow(1);    // show ORE windows
                ORE.ProblemDirectory = value;
                LoadLastMeasurement();  // load a default page
                ORE.LoadProblemConfigurationFiles();
                Problem = ORE.ProblemDirectory;
                RaiseDirChangeEvent(); //
                ORE.FittingWindow.Visible = true;
                ORE.FittingWindow.Silent = true;
                ShowWindow(2);    // hide ORE windows
            }

        }
        get
        {
            return Problem;
        }
    }

   
    public static string rootDirectory => ORE.RootDirectory;    //!< return slected root directory

    /// <summary>
    /// set the fitting to silent / no chart updates
    /// </summary>
    public static bool SetFittingSilent
    {
        get { return ORE.FittingWindow.Silent; }
        set
        {
            ORE.FittingWindow.Silent = value;
        }
    }

    /// <summary>
    /// set or reset reflection without backside
    /// </summary>
    public static bool NoRbackside
    {
        get
        {
            return ORE.Data.R_WithoutBackSide;
        }
        set
        {
            ORE.Data.R_WithoutBackSide = value;
        }
    }


    /// <summary>
    /// set or remove T_WithoutBackside
    /// </summary>
    public static bool NoTbackside
    {
        get
        {
            return ORE.Data.T_WithoutBackSide;
        }
        set
        {
            ORE.Data.T_WithoutBackSide = value;
        }
    }

    /// <summary>
    /// list of index models avialable
    /// </summary>
    /// <returns>list of index models in OptiRE</returns>
    public static string[] nModels()
    {
        string[] nm = doptire.NSysModels.GetNames(typeof(NSysModels));
        return nm;
    }

    /// <summary>
    /// extinction models in OptiRE
    /// </summary>
    /// <returns>list of extinction model types</returns>
    public static string[] kModels()
    {
        string[] nk = doptire.KSysModels.GetNames(typeof(KSysModels));
        return nk;
    }

    /// <summary>
    /// OPtiRE measurement correction types
    /// </summary>
    /// <param name="ctp">type of correction</param>
    /// <returns>OptiRE enum name of correction</returns>
    public static string OreCorrectionType( string ctp)
    {
        var otp = CorrTypes.ctNoCorr.ToString();
        switch (ctp)
        {
            case "Constant":
                otp = CorrTypes.ctConst.ToString();
                break;
            case "Linear":
                otp = CorrTypes.ctLinear.ToString();
                break;
            case "Quadratic":
                otp = CorrTypes.ctQuadratic.ToString();
                break;
            default:
                otp = CorrTypes.ctNoCorr.ToString();
                break;
        }
        return otp;
    }


    /// <summary>
    /// class to send spectral data sets in memory
    /// avoids filling up the measurement database
    /// </summary>
    [SupportedOSPlatform("Windows7.0")]
    public static class SendInMemory
    {
        static List<MeasurementSet> MsrMnts = new();

        /// <summary>
        /// Clear all pending spectral data from list
        /// </summary>
        public static void ClearMeasurements() { MsrMnts.Clear(); }

        /// <summary>
        /// Add a set of measurement spectra to be transferred to OptiRe in memory
        /// </summary>
        /// <param name="points">nbr of spectral points</param>
        /// <param name="Angle">incidence angle</param>
        /// <param name="MeasType">type of measurement T/R/BR</param>
        /// <param name="wvl">wavelenght array</param>
        /// <param name="Intensity">intensity array</param>
        /// <param name="Tolerance">tolerance array</param>
        /// <returns>nbr of data in measurement list</returns>
        public static int AddSpectra(int points, double Angle, string MeasType, Array wvl, Array Intensity, Array Tolerance)
        {
            MsrMnts.Add(new MeasurementSet()
            {
                points = points,
                Angle = Angle,
                MsrmtType = MeasType,
                Lambda = wvl,
                Intensity = Intensity,
                Tolerance = Tolerance
            });
            return MsrMnts.Count;
        }

        
        /// <summary>
        /// load a measurement page after a specified design layer
        /// default is saving after last layer
        /// </summary>
        /// <param name="afterlayer">design layer after which to load</param>
        /// <param name="save">save measurements to OptiRE database ?</param>
        /// <param name="Title">title of measurement set</param>
        /// <param name="Comment">comment for data</param>
        /// <returns></returns>
        public static bool LoadMeasPage(int afterlayer, bool save, string Title, string Comment)
        {
            bool isOK = false;
            // create the measurement page structure
            var PageOpen = ORE.Databases.MeasurementDatabase.CreateMeasurement(out Measurement mpage);
            mpage.Name = Title;
            if (PageOpen)
            {
                foreach (var item in MsrMnts)
                {
                    isOK = mpage.AddMeasurementPage(item.points, item.Angle, item.MsrmtType, item.Lambda, item.Intensity, item.Tolerance);
                    if (!isOK)
                    {
                        OptiMessageSend("OptiRE data transfer error: " + ORE.LastErrorText);
                    }
                }
                MsrMnts.Clear(); // empty the list of data
                // load the page
                afterlayer = afterlayer > DesignLayers() || afterlayer <= 0 ? DesignLayers() : afterlayer;
                isOK = mpage.LoadAfterLayer(afterlayer);
                if (save)
                {
                    mpage.Save(Title, Comment);
                }
            }
            return isOK;
        }
            
            
        }

    

    /// <summary>
    /// send a set of spectral data to OptiRE database
    /// </summary>
    /// <param name="wvl">wavelength</param>
    /// <param name="Intensity">intensity</param>
    /// <param name="Tolerance">Tolerance</param>
    /// <param name="angle">incidence angle</param>
    /// <param name="MeasType">type of spectra (Ta,Ra,BRA..)</param>
    /// <param name="append">append to current page</param>
    /// <param name="Name">Name of page</param>
    /// <param name="comment">comment</param>
    /// <returns></returns>
    public static bool SendSpectralSet(Array wvl, Array Intensity, Array Tolerance, double angle, string MeasType, bool append, string Name, string comment)

    {
        var npnts = wvl.Length;
        bool isOK = ORE.Databases.MeasurementDatabase.LoadMeasurementPage(npnts, angle, MeasType, wvl, Intensity, Tolerance, append, Name, comment);
        LastMeasSet = isOK ? Name : LastMeasSet;
        return isOK;

    }

    /// <summary>
    /// load a measurement page from database
    /// </summary>
    /// <param name="MsrName">name of measurement page</param>
    /// <param name="pos">position in stack</param>
    /// <returns></returns>
    public static bool LoadMeasurement(string MsrName, int pos)
    {
        bool res;
        if (pos <=0 || pos >= DesignLayers())
        {
            // assume normal last layer 
            res = ORE.Databases.MeasurementDatabase.Load(MsrName);
        }
        else
        {
            res = ORE.Databases.MeasurementDatabase.LoadAfterLayer(MsrName, pos);
        }
        if (!res)
        {
            ORE_error();
        }
        return res;
    }


    /// <summary>
    /// Load a theoretical design to ORE database
    /// </summary>
    /// <param name="nLayer">nbr of layers</param>
    /// <param name="thickn">array of thickness values</param>
    /// <param name="Abbr">array of Material abbreviations</param>
    /// <param name="name">Design Name</param>
    /// <param name="comment">Comment</param>
    /// <returns></returns>
    public static bool LoadThDesign(int nLayer, double[] thickn, string[] Abbr, string name, string comment)
    {
        Array th_O = (Array)thickn;
        Array abr_O = (Array)Abbr;
        bool isOK = true;
        try
        {
            ORE.Data.LoadTheoreticalDesign(550.0, 0.0, 1.0, nLayer, th_O, abr_O, name, comment);

        }
        catch (Exception)
        {
            OptiMessageSend("Error Creating Theoretical Design: " + name);
        }
        if (!isOK) { ORE_error(); }
        return isOK;
    }

    /// <summary>
    /// Load a material definition based on a Cauchy model to OptiRe
    /// </summary>
    /// <param name="npar">array(3)holds n parameters </param>
    /// <param name="kpar">array(3) holds k parameters (exp)</param>
    /// <param name="Abbr">Abbreviation to use</param>
    /// <param name="name">Name</param>
    /// <param name="comment">Comment</param>
    /// <returns></returns>
    public static bool LoadCauchyFormula(double[] npar, double[] kpar, string Abbr, string name, string comment)
    {
        Array n_O = (Array)npar;
        bool isOK = false;
        // k is based on an exponential using 3 parameters
        if (kpar != null)
        {
            Array kpar_O = (Array)kpar;
            isOK = ORE.Data.LoadMaterialFormula(nType.CAUCHY, n_O, kType.EXPONENTIAL, kpar_O, Abbr, name, comment);
            if (!isOK) { ORE_error(); }
        }
        else        //no absoption
        {
            isOK = ORE.Data.LoadMaterialFormula(nType.CAUCHY, n_O, kType.NOABS, null, Abbr, name, comment);
            if (!isOK) { ORE_error(); }
        }
        return isOK;
    }

    /// <summary>
    /// load a Material from Database to memory
    /// </summary>
    /// <param name="Name"></param>
    /// <param name="Abbr"></param>
    /// <returns>success</returns>
    public static bool LoadMaterial(string Name, string Abbr)
    {
        bool isOK = ORE.Databases.MaterialDatabase.LoadMaterial(Name, Abbr);
        if (!isOK) ORE_error();
        return isOK;
    }

    /// <summary>
    /// load a substrate string to OptiRE
    /// </summary>
    /// <param name="sub">substrate name to load</param>
    /// <returns>success</returns>    
    public static bool LoadSubstrate(string sub)
    {
        REDatabase sdb = ORE.Databases.get_Database(DatabaseKind.dbSubstrate);
        //			bool res = ORE.Databases.get_Database(DatabaseKind.dbSubstrate).Load(sub);
        bool res = sdb.Load(sub);
        if (!res)
        {
            ORE_error();
        }
        else ORE.Data.R_WithoutBackSide = false;        // make sure backside is disabled
        return res;

    }

    /// <summary>
    /// load an incidence medium
    /// </summary>
    /// <param name="sub">name of medium/substrate</param>
    /// <returns>success</returns>
    public static bool LoadIncMedium(string sub)
    {
        // load a substrate string to OptiRE
        REDatabase sdb = ORE!.Databases.get_Database(DatabaseKind.dbIncMedium);
        bool res = sdb.Load(sub);
        if (!res) ORE_error();
        return res;
    }

    /// <summary>
    /// load a substrate as exit medium for the current design
    /// </summary>
    /// <param name="sub">substrate / medium name</param>
    /// <returns>success</returns>
    public static bool LoadExitMedium(string sub)
    {
        // load a substrate string to OptiRE
        REDatabase sdb = ORE!.Databases.get_Database(DatabaseKind.dbExitMedium);
        bool res = sdb.Load(sub);
        if (!res) ORE_error();
        return res;
    }

    /// <summary>
    /// load substrate thickness
    /// </summary>
    /// <param name="ds">thickness in mm</param>
    public static void LoadSubstrateThickness(double ds)
    {
        ORE.Data.SubstrateThickness = ds;
    }

    public static double SubstrateThickness
    {
        get
        {
            return ORE.Data.SubstrateThickness;
        }
        set
        {
            ORE.Data.SubstrateThickness = value;
        }
    }

    /// <summary>
    /// load design to OptiRE
    /// </summary>
    /// <param name="desn">design name from database</param>
    /// <param name="clrSpectra">clear intermediate targets</param>
    /// <returns>the design name loaded</returns>
    public static string LoadDesign(string desn, bool clrSpectra)
    {
        string desname = "";    // first remember the currently loaded design
        bool res = ORE.Data.GetLoadedName(DatabaseKind.dbDesign, null, out desname);
        if (desn != desname)
        {
            try
            {
                res = ORE.Databases.DesignDatabase.Load(desn);
                if (res) desname = desn;
            }
            catch
            {
                // in case of error re-load the old design to avoid issues
                res = ORE.Databases.DesignDatabase.Load(desname);
            }
            if (!res) ORE_error();
        }
        else
        {
            ORE.Data.ClearModel();
        }
        return desname;
    }

    /// <summary>
    /// remove all intermediate measurement spectra
    /// </summary>
    public static void RemoveIntermediates()
    {
        for (int i = 0; i < DesignLayers(); i++)
        {
            ORE.Data.RemoveMeasurement(i);
        }
    }

    /// <summary>
    /// load a backside design
    /// </summary>
    /// <param name="ds">design name </param>
    /// <returns></returns>
    public static string LoadBacksideDesign(string ds)
    {
        bool res = false;
        try
        {
            res = ORE.Databases.DesignDatabase.LoadBackDesign(ds);
            if (res) return ds; else return "";
        }
        catch (COMException)
        {
            VersionError();
            return "";
        }
    }

    public static bool RemoveBacksideDesign()
    {
        bool res = false;
        try
        {
            res = ORE.Data.RemoveBackSideDesign();
        }
        catch (Exception)
        {
            //        		VersionError();  no message if old version, just ignore it
            res = false;
        }
        return res;
    }

    /// <summary>
    /// get current loaded design
    /// </summary>
    public static string CurrentDesign
    {
        get
        {
            bool res = ORE.Data.GetLoadedName(DatabaseKind.dbDesign, null, out string dn);
            if (res) return dn; else return null;
        }
    }

    public static string CurrentSubstrate
    {
        get
        {
            bool res = ORE.Data.GetLoadedName(DatabaseKind.dbSubstrate, null, out string dn);
            if (res) return dn;
            else return null;

        }
    }


    /// <summary>
    /// calculate internal transmission data for a given substrate medium
    /// used mainly for PVB interleaves: 300 to 2500 nm range in 5 nm steps
    /// </summary>
    /// <param name="substrate">name of substrate in OptiRE</param>
    /// <param name="thickness">thickness of the substarte in mm</param>
    /// <returns>wavelength-intensity or null-null if fails</returns>
    public static (double[] wl, double[] val) GetInternalTran(string substrate, double thickness)
    {
        var sub = ORE_IF.CurrentSubstrate;
        var sd = ORE_IF.SubstrateThickness;
        var slist = ORE_IF.SubstrateList();
        if(!slist.Contains(substrate))
        {
            return (null,null);
        }
        ORE_IF.LoadSubstrate(substrate);
        var ww = new List<double>();
        var wstart = 300.0;
        do
        {
            ww.Add(wstart);
            wstart += wstart < 750 ? 5.0 : 10.0;

        } while (wstart <2500);
        var wvl = ww.ToArray();
        double[] Tran = new double[wvl.Length];
        Tran.Initialize();
        var dsg = ORE_IF.CurrentDesign; // save the current 
        ORE_IF.SubstrateThickness = thickness;
        ORE_IF.LoadIncMedium(substrate);
        ORE_IF.LoadExitMedium(substrate);
        CreateMateDesign(0.5); // create a dummy design
        var isOK = ORE_IF.GetCharacteristic(wvl.Length, ORECharNames.Ta, 0.0, wvl, ref Tran);
        if (isOK)
        {
            for (int i = 0; i < Tran.Length; i++)
            {
                Tran[i] /= 100.0; // convert to generic
            }
        }
        // re-load the original design and substrate
        ORE_IF.LoadIncMedium("Air");
        ORE_IF.LoadExitMedium("Air");
        ORE_IF.LoadDesign(dsg, true); // 
        ORE_IF.LoadSubstrate(sub);
        ORE_IF.SubstrateThickness = sd;
        return isOK ? (wvl,Tran) : (null,null);
    }

    /// <summary>
    /// loads or creates a new dummy design for tin-side surface
    /// for a bare glass used as mate glass in IG units
    /// </summary>
    /// <returns>success</returns>
    public static bool CreateMateDesign(double thickness)
    {
        string dsgn = "MateGlass";
        string MatID = "MG"; // OptiRE Material ID for Mate dummy tin side layer
        string MatName = "TinSideMate"; // OptiRe Database Material Name
        bool isOK = false;
        // try to load default design
        try
        {
            isOK = ORE.Databases.DesignDatabase.Load(dsgn);
        }
        catch (Exception)
        {
            string sb = "";
            bool isSub = ORE.Data.GetLoadedName(DatabaseKind.dbDesign, null, out sb);
            OptiMessageSend("Error loading Mate Design: " + sb);
            isOK = (sb == dsgn);
            // throw;
        }
        //			isOK = ORE.Databases.get_Database(DatabaseKind.dbDesign).Load(Design);
        if (isOK)
        {
            // verify thickness: to correct old designs/materials
            double[] dth = GetThickness();
            isOK = dth[0] == thickness;
        }
        if (!isOK)
        {
            // define material and design and load it to OptiRe
            double[] cpar = { 1.620000, 2.1860900e-3, 1.6842273e-4 }; // this is a cauchy model for SiONy (1.6)
            isOK = LoadCauchyFormula(cpar, null, MatID, MatName, "Generated SiOxNy material for tin side");
            isOK = LoadMaterial(MatName, MatID);
            double[] th = { thickness };
            string[] Abbr = { MatID };
            Int32 nl = 1;
            isOK = LoadThDesign(nl, th, Abbr, dsgn, "Mate GLass design");
            string nds = LoadDesign(dsgn, true); // load it to memory
        }
        return isOK;
    }

    /// <summary>
    /// delete a design from the database
    /// </summary>
    /// <param name="design">design to remove</param>
    /// <returns>success</returns>
    public static bool DeleteDesign(string design)
    {
        //			bool res = ORE.Databases.get_Database(DatabaseKind.dbDesign).Delete(design);
        bool res = ORE.Databases.DesignDatabase.Delete(design);
        if (!res) ORE_error();
        return res;
    }

    public static bool ResetModel()     //!< OBSOLETE	 DOES NOT EXIST anymore
    {
        
        bool res = false;
        try
        {
            res = ORE.Data.ClearModel();
            return true;
            // TODO: verify the ResetModel function
        }
        catch (Exception)
        {
            return false;
        }
    }

    public static int DesignLayers()    //!< number of layers in current design
    {
        return ORE.Data.N_of_Layers;
    }

    /// <summary>
    /// get the n and k values at a specific wavelength for a given material
    /// </summary>
    /// <param name="mat">material name</param>
    /// <param name="wvl">wavelength </param>
    /// <param name="ndx">refractive index</param>
    /// <param name="nk">extinction coefficent</param>
    public static void GetNK_Data(string mat, double wvl, ref double ndx, ref double nk)
    {
        // wavelength  may be in in mue - only in Versions before 7.68!
        wvl = ORE.Data.U_Length == LengthUnits.L_MKM ? wvl / 1000 : wvl;
        // avoid the change made in V7.68 to nm scale
        //			ORE.Data.U_Length = LengthUnits.L_MKM;  // sets length units to MKM i.e. microns
        bool res = ORE.Data.GetMaterial_nk(mat, wvl, out ndx, out nk);
        //			ORE.Data.U_Length = LengthUnits.L_NM;       // reset to nm
        if (!res) ORE_error();
    }

    /// <summary>
    /// get the index table for a given material
    /// </summary>
    /// <param name="mat">name of material</param>
    /// <param name="wvl">array of wavelenghth to get at </param>
    /// <param name="ndx">index value</param>
    /// <returns>success</returns>
    public static bool GetLoadedNTable(string mat, out double[] wvl, out double[] ndx)
    {
        // retrieve the index table for a given material
        //			double[] wavel = new double[500] , index = new double[500];
        object wavel = new double[500];
        object index = new double[500];
        nType ndxtype;
        int ndata = 0;
        bool res = ORE.Data.GetLoadedMaterial_n_Table(mat, out ndxtype, out ndata, ref wavel, ref index);
        wvl = new double[ndata];
        ndx = new double[ndata];
        for (int i = 0; i < ndata; i++)
        {
            wvl[i] = ((double[])wavel)[i];
            ndx[i] = ((double[])index)[i];
        }
        return res;
    }

    /// <summary>
    /// returns a double[] with thickness data for current design
    /// </summary>
    /// <returns>array of thickness values</returns>
    public static double[] GetThickness()
    {
        // get thickness data from ORE
        int nl = DesignLayers(); // get number of layers of loaded design
        double[] th = new double[nl];
        for (int i = 0; i < nl; i++)
        {
            th[i] = -1.0;
        }
        Object ttl = th;
        bool res = ORE.Data.GetThicknesses(ref ttl);
        if (!res) ORE_error();
        th = (double[])ttl;
        return th;
    }

    /// <summary>
    /// returns a double[] with thickness deviation data for current fit
    /// </summary>
    /// <returns>array of percent deviations </returns>
    public static double[] GetThicknessDeviation()
    {
        // get thickness data from ORE
        int nl = DesignLayers(); // get number of layers of loaded design
        double[] th = new double[nl];
        for (int i = 0; i < nl; i++)
        {
            th[i] = -1.0;
        }
        Object ttl = th;
        bool res = ORE.Data.GetRelThickErrors(ref ttl);
        if (!res) ORE_error();
        th = (double[])ttl;
        return th;
    }

    /// <summary>
    /// get the material abbreviation for layer n in loaded stack
    /// </summary>
    /// <param name="layer">the index of the layer </param>
    /// <returns></returns>
    public static string GetMatAbbreviation(int layer)
    {
        string abbr = "";
        int nl = DesignLayers();
        if (layer <= nl) abbr = ORE.Data.get_MaterialAbbreviation(layer);
        return abbr;
    }


    /// <summary>
    /// a delay to wait until OptiRE is ready
    /// </summary>
    /// <returns>OptiRE ready status</returns>
    public static bool WaitForOptiRE()
    {
        int cnt = 19;   // max wait periods of 10 ms each
        while (cnt > 0 && !ORE.Data.IsDataReadyForRE)
        {
            Task tsk = Task.Delay(OreDelay);
            tsk.Wait();
        }
        return cnt > 0 ? true : false;
    }

    /// <summary>
    /// start a random error optimization using the loaded data and design
    /// </summary>
    /// <param name="tol">tolerances array</param>
    /// <param name="free">free layers array</param>
    /// <param name="ctype">type of OptiRE measurement correction</param>
    /// <returns>success</returns>
    public static bool RandomErrorFit(double[] tol, bool[] free, CorrTypes ctype)
    {
        if (!WaitForOptiRE())
        {
            OptiMessageSend("ORE is not Ready for optimization!: OptiRE Random");
            return false;
        }
        //			ORE.FittingWindow.Silent = true;	// avoid replotting
        Array tol_O = (Array)tol;
        Array fr_O = (Array)free;
        bool doCorrection = ctype != CorrTypes.ctNoCorr;
        bool isOK = ORE.Data.RE_RandomErrors(tol_O, fr_O, doCorrection, ctype,
                                             ctype == CorrTypes.ctConst ? CorrKinds.ckShift : CorrKinds.ckScale);
        if (reMinimize)
        {
            MinimizeOptiRE();
        }
        if (!isOK)
        {
            ORE_error();
        }
        return isOK;
    }


    /// <summary>
    /// random error extended method: free also some index parameter in fit
    /// </summary>
    /// <param name="tol">tolerance array</param>
    /// <param name="free">array to flag thickness vary on layers</param>
    /// <param name="matab">material abbreviation array</param>
    /// <param name="nmodl">index model to use</param>
    /// <param name="kmodl">extinction model to use</param>
    /// <param name="ActvMod">list to flag index active layers</param>
    /// <param name="octype">OptiRE correction type</param>
    /// <returns>success</returns>
    public static bool RandomErrorExtended(double[] tol, bool[] free, List<string> matab,
                                           List<int> nmodl, List<int> kmodl, List<bool> ActvMod, OptiCorrections octype)
    {
        int nvar = matab.Count;
        CorrTypes ctype = (CorrTypes) octype;   // convert it back to local Optilayer type      
        if (!ActvMod.Contains(true))
        {
            // just run a normal thickness fit if no materials are marked as variant
            return RandomErrorFit(tol, free, ctype);
        }
        //			ORE.FittingWindow.Silent = true;
        int[] ThMod = new int[nvar];
        bool[] ModAct = new bool[nvar];
        bool[] ThActv = new bool[nvar];
        for (int i = 0; i < nvar; i++)
        {
            ThMod[i] = (int)SysModels.smNoErr;
            ThActv[i] = false;
        }
        string[] MatAb = new string[nvar];
        int[] nModel = new int[nvar], kModel = new int[nvar];
        matab.CopyTo(MatAb);
        nmodl.CopyTo(nModel);
        kmodl.CopyTo(kModel);
        ActvMod.CopyTo(ModAct);
        if (!WaitForOptiRE())
        {
            OptiMessageSend("ORE is not Ready for optimization!: OptiRE Random");
            return false;
        }
        bool isOK = ORE.Data.RE_RandomErrorsEx((Array)tol, (Array)free, nvar, (Array)MatAb,
                                               (Array)ThMod, (Array)ThActv, (Array)nModel, (Array)ModAct, (Array)kModel,
                                               (Array)ModAct, false, ctype,
                                               ctype == CorrTypes.ctConst ? CorrKinds.ckShift : CorrKinds.ckScale);
        if (reMinimize)
        {
            MinimizeOptiRE();
        }
        if (!isOK)
        {
            ORE_error();
        }
        return isOK;
    }


    /// <summary>
    /// random extended optimization for single layer design
    /// for layer VarMatLay vary n & k, all others just d
    /// </summary>
    /// <param name="VarMatLay">variant layer </param>
    /// <param name="Mat">material name</param>
    /// <param name="rdev">deviation tolerance band</param>
    /// <param name="nModel">index model</param>
    /// <param name="kModel">extinction model</param>
    /// <returns>success</returns>
    public static bool RandomOptim_SL(int VarMatLay, string Mat, double rdev, string nModel, string kModel)
    {
        // start an extended random Optimazation: for layer VarMatLay vary n & k, all others just d
        if (!WaitForOptiRE())
        {
            OptiMessageSend("ORE is not Ready for optimization!: OptiRE Random EX Optim.");
            return false;
        }
        //			ORE.FittingWindow.Silent = true;
        int nlay = ORE.Data.N_of_Layers;
        double[] reldev = new double[nlay];
        bool[] bLayAct = new bool[nlay];
        for (int i = 0; i < nlay; i++)
        {
            reldev[i] = rdev;
            bLayAct[i] = true;
        }
        int varMat = 1;
        string[] MatAbr = new string[varMat];
        MatAbr[0] = Mat;
        int[] ThMod = new int[varMat];
        int[] nMod = new int[varMat];
        int[] kMod = new int[varMat];
        bool[] ModAct = new bool[varMat];
        bool[] ThAct = new bool[varMat];
        for (int i = 0; i < varMat; i++)
        {
            ThMod[i] = (int)SysModels.smNoErr;
            ModAct[i] = true;
            ThAct[i] = false;
            nMod[i] = (int)NSysModels.nsLoaded;
            kMod[i] = (int)KSysModels.ksLoaded;
        }
        nMod[VarMatLay] = (int)NModel(nModel);
        kMod[VarMatLay] = (int)KModel(kModel);
        bool res = ORE.Data.RE_RandomErrorsEx((Array)reldev, (Array)bLayAct, varMat, (Array)MatAbr,
                                              (Array)ThMod, (Array)ThAct, (Array)nMod, (Array)ModAct, (Array)kMod, (Array)ModAct,
                                              false, CorrTypes.ctNoCorr, CorrKinds.ckShift);
        if (reMinimize)
        {
            MinimizeOptiRE();
        }
        if (!res)
        {
            ORE_error();
        }
        return res;

    }


    /// <summary>
    /// obtain calculated spectral data for current model in OptiRe
    /// </summary>
    /// <param name="nPoints">number of points</param>
    /// <param name="ch">characteristic T/R/BR</param>
    /// <param name="angle">incidence angle</param>
    /// <param name="wvl">wavelength array</param>
    /// <param name="val">intensity array</param>
    /// <returns></returns>
    public static bool GetCharacteristic(int nPoints, string ch, double angle, double[] wvl, ref double[] val)
    {
        bool isOK = GetCharacteristic(nPoints, ch, angle, -1, wvl, ref val);    // call detail with all layers
        return isOK;
    }

    /// <summary>
    /// obtain calculated spectral data for current model in OptiRe
    /// </summary>
    /// <param name="nPoints">number of points</param>
    /// <param name="ch">type of characteristic T/R/RB</param>
    /// <param name="angle">incidence angle</param>
    /// <param name="afterLayer">characteristic after design layer </param>
    /// <param name="wvl">wavelength array</param>
    /// <param name="val">intensity array</param>
    /// <returns>success</returns>
    public static bool GetCharacteristic(int nPoints, string ch, double angle, int afterLayer, double[] wvl, ref double[] val)
    {
        Array wvl_O = (Array)wvl;
        Object val_O = (Array)val;
        bool isOK = ORE.Data.GetChar(nPoints, angle, ch, afterLayer, false, wvl_O, ref val_O);
        if (!isOK) ORE_error();
        val = (double[])val_O;
        return isOK;
    }

    /// <summary>
    /// convert index model names to OptiRE enum
    /// </summary>
    /// <param name="nmod">index model name</param>
    /// <returns></returns>
    private static NSysModels NModel(string nmod)
    {
        NSysModels nm = NSysModels.nsNormal;
        switch (nmod)
        {
            case "nsArbitrary":
                nm = NSysModels.nsArbitrary;
                break;
            case "ns_nLam":
                nm = NSysModels.ns_nLam;
                break;
            default:
                nm = NSysModels.nsNormal;
                break;
        }
        return nm;
    }

    /// <summary>
    /// convert extinction model name to OptiRE enum
    /// </summary>
    /// <param name="kmod">k model name</param>
    /// <returns></returns>
    private static KSysModels KModel(string kmod)
    {
        KSysModels km = KSysModels.ksLoaded;
        switch (kmod)
        {
            case "ks_UV_VIS":
                km = KSysModels.ks_UV_VIS;
                break;
            case "ks_kLam":
                km = KSysModels.ks_kLam;
                break;
            case "ks_NonAbs":
                km = KSysModels.ksNonAbs;
                break;
            default:
                km = KSysModels.ks_UV_VIS;
                break;
        }
        return km;
    }


    public static double GetDiscrepancy()   //!< fitting discrepancy of last match
    {
        return ORE.Data.Discrepancy;
    }

    /// <summary>
    /// return the details of the design loaded in OPtiRE
    /// </summary>
    /// <returns>a design class structure</returns>
    public static Design GetActiveDesign()
    {
        string desname = "";
        bool isOK = ORE.Data.GetLoadedName(DatabaseKind.dbDesign, null, out desname);
        int nlay = DesignLayers();
        double[] dnm = new double[nlay];
        string[] MatAbbr = new string[nlay];
        Design ActiveDesign = new Design(desname);
        // clear any optimization to get base model
        //ResetModel();
        object ld = dnm;        // need to convert to object for COM
        object ma = MatAbbr;
        isOK = ORE.Data.GetThicknesses2(ref ld, ref ma);
        if (!isOK) ORE_error();
        dnm = (double[])ld;
        MatAbbr = (string[])ma;
        for (int i = 0; i < nlay; i++)
        {
            DesignLayer dl = new DesignLayer(i + 1, dnm[i], MatAbbr[i], 1.0);
            dl.isVar = dnm[i] < 6.0 ? false : true;
            ActiveDesign.LayerList.Add(dl);
        }
        isOK = ORE.Data.GetLoadedName(DatabaseKind.dbSubstrate, null, out desname);
        if (!isOK) ORE_error();
        ActiveDesign.Substrate = desname; // get the substrate name
        ActiveDesign.SubThickness = ORE.Data.SubstrateThickness;
        return ActiveDesign;
    }

    
    private static void ORE_error()
    {
        // get last ORE Error and display a message:
        string Oerr = ORE.LastErrorText;
        OptiMessageSend("ORE Error: " + Oerr);
    }

    private static void VersionError()
    {
        // get current version code and display a message:
        string Oerr = "not supported in OptiRE: " + OptiVersion;
        OptiMessageSend("unsupported in " + OptiVersion);
    }

    private static void UnsupportedMessage()
    {
        OptiMessageSend("function not supported");
    }

    private static string CharToString(char[] cary)        // convert a char array to a string
    {
        string rs = "";
        for (int i = 0; i < cary.Length; i++) rs = rs + cary[i];
        return rs;
    }

    /// <summary>
    /// Get the name list of Database Items of a OptiRe database (Designs, Substrates)
    /// </summary>
    /// <param name="dbn"> name of Database</param>
    /// <returns>a list titles</returns>
    private static List<string> OreDbItems(string dbn)
    {
        List<string> namelist = new List<string>();
        string ffname = Path.Combine(RootDir, Problem);    // make the full path
        ffname = Path.Combine(ffname, dbn);
        if (File.Exists(ffname))
        {
            // open the file using a binary reader and scan the entries
            FileInfo fi = new FileInfo(ffname);
            FileStream fs = fi.OpenRead();
            BinaryReader br = new BinaryReader(fs, System.Text.Encoding.UTF8);
            byte[] dbuf = new byte[18];     // this is just a read buffer for the datetime structures
            char[] cary = br.ReadChars(7);
            if (br.PeekChar() > 0)      // make sure the file is not empty
            {
                int dcntr = br.ReadInt32();     // get the type counter
                Int16 rcntr = br.ReadInt16();   // get the record counter
                string txt;
                for (int i = 0; i < rcntr; i++)    // read all records
                {
                    if (br.PeekChar() < 0) break;       // unexpected end of file
                    Int16 rn = br.ReadInt16();      // record ID
                    int cntr = br.ReadInt32();      // get the string length
                    dbuf = br.ReadBytes(cntr);      // get the name of the design
                    if (dbuf.Length == 0) break;        // no more data ?
                    txt = Encoding.UTF8.GetString(dbuf);    // convert the bytes to a string
                                                            //cary = br.ReadChars(cntr);
                    namelist.Add(txt);  // put the name into the string list
                                        // need to read the rest of the record off the stream:
                                        // would need to use Encoding Conversion to get it to as string "ISO-8859-1"
                    br.ReadBytes(18);       // read a date time structure: createt
                    cntr = br.ReadInt32();      // get the string length
                    dbuf = br.ReadBytes(cntr);  // get the user name
                    br.ReadBytes(18);               // modified date time
                    cntr = br.ReadInt32();              // get the string length
                    dbuf = br.ReadBytes(cntr);          // get the user name
                                                        //txt = Encoding.GetEncoding("ISO-8859-1").GetString(dbuf);    // convert the bytes to a string
                    cntr = br.ReadInt32();      // get the string length
                    dbuf = br.ReadBytes(cntr);  // get the comment
                }
            }
            fs.Close();
        }
        else
        {
            namelist.Add("NA");
            OptiMessageSend("Can'find database item" + dbn);
        }
        return namelist;
    }

    /// <summary>
    /// returns the list of designs in the current problem directory
    /// </summary>
    /// <returns>list of design names</returns>
    public static List<string> DesignList()
    {
        int nd = ORE.Databases.DesignDatabase.FileListCount;
        string[] ll = new string[nd];
        object pp = (Array)ll;
        bool success = ORE.Databases.DesignDatabase.FileList(ref nd, ref pp);
        ll = (string[])pp;
        List<string> sl = new List<string>();
        for (int i = 0; i < nd; i++)
        {
            sl.Add(ll[i]);
        }
        //OreDbItems("DESIGNA.DBS");  --> calling OptiRE directly 31-May-2012
        sl.Remove("MateGlass");
        sl.Sort();
        return sl;
    }

    /// <summary>
    /// returns the list of substrates in the current problem directory
    /// </summary>
    /// <returns>substrate names</returns>
    public static List<string> SubstrateList()
    {
        // load a substrate string to OptiRE
        REDatabase sdb = ORE.Databases.get_Database(DatabaseKind.dbSubstrate);
        //			int ns = ORE.Databases.get_Database(DatabaseKind.dbSubstrate).FileListCount;
        int ns = sdb.FileListCount;
        if (ns < 2)
        {
            // there may be always the "Air" loaded as substrate
            OptiMessageSend("Can't find substrate materials in PD: " + ORE.ProblemDirectory);
        }
        string[] ll = new string[ns];
        object pp = (Array)ll;
        //			bool success = ORE.Databases.get_Database(DatabaseKind.dbSubstrate).FileList(ref ns, ref pp);
        sdb.FileList(ref ns, ref pp);
        ll = (string[])pp;
        List<string> sl = new List<string>();
        for (int i = 0; i < ns; i++)
        {
            if (ll[i].ToUpper() != "AIR") sl.Add(ll[i]);
        }
        sl.Sort();
        return sl;
    }

    private static void ShowWindow(int state)
    {
        Process[] processes = Process.GetProcessesByName("doptire");
        foreach (Process p in processes)
        {
            System.IntPtr app_hwnd;
            WINDOWPLACEMENT wp = new WINDOWPLACEMENT();
            app_hwnd = p.MainWindowHandle;
            GetWindowPlacement(app_hwnd, ref wp);
            wp.showCmd = state; // 1- Normal; 2 - Minimize; 3 - Maximize;
            SetWindowPlacement(app_hwnd, ref wp);
        }
    }


}


/// <summary>
/// holds a reference to the refractive index at specific wavelength
/// </summary>
public struct IndexReference
{
    public double wvl;
    public double ndx;
    public double ext;
    public IndexReference(double w, double n, double k)
    {
        wvl = w;
        ndx = n;
        ext = k;
    }
}


public struct IndexModels
{
    public List<string> material;   //!< material abbreviations
    public List<int> n_model;       //!< index type model
    public List<int> k_model;       //!< extinction type model
    public List<bool> active;       //!< model for layer is free to move
}
/// <summary>
/// holds information on a Optilayer design: 
/// this is normally read back from OptiRe
/// is used to modify a design in thickness or index
/// </summary>
public class Design
{
    private string DName = "";  // design Name
    private string Substr = "";  // loaded Substrate name
    private double dSubstr = 4.0;     // substrate thickness
    //private double normalEmissivity = 0.837;    // can hold an emissivity for the layer
    public double RefWavelength { get; set; }       //!< reference wavlength for index retrieval
    public double fitDiscrepancy { get; set; }      //!< holds the dicrepancy after a fit
    public int measPosition { get; set; }           //!< list position of measurement reference
    public double[] ModelMSE { get; set; }     //!< may hold the MSE measured spectra vs base model
    public List<DesignLayer> LayerList = new(); //!< list of layers and layer properties of design
    public Dictionary<string, IndexReference> IndexResults = new ();


    /// <summary>
    /// create a new default design structure
    /// </summary>
    /// <param name="Name">design name </param>
    [SupportedOSPlatform("Windows7.0")]
    public Design(String Name)
    {
        // create a new design structure with nl
        DName = Name;
        RefWavelength = 550.0;
        fitDiscrepancy = 0;
        MeasCorr = ORE_IF.OptiCorrections.ctNoCorr;
        measPosition = -1;
    }

    /// <summary>
    ///  the name of the design
    /// </summary>
    public string DesignName
    {
        get { return DName; }
        set { DName = value; }
    }

    /// <summary>
    ///  currently loaded substrate
    /// </summary>
    public string Substrate
    {
        get { return Substr; }
        set { Substr = value; }
    }

    /// <summary>
    /// get / set substrate thickness
    /// </summary>
    public double SubThickness
    {
        get { return dSubstr; }
        set { dSubstr = value; }
    }

    /// <summary>
    /// number of layers in stack
    /// </summary>
    public int NoLayers
    {
        get { return LayerList.Count; } 
    }

    /// <summary>
    /// get the number of free layers in design
    /// </summary>
    public int FreeLayers
    {
        get
        {
            var fl = 0;
            foreach (var item in LayerList)
            {
                if (item.isVar)
                {
                    fl++;
                }
            }
            return fl;
        }
    }

    public double EpsNormal { get; set; } //!< set or get normal emissivity of design
   
    /// <summary>
    /// get the thickness of the layers in the design
    /// </summary>
    /// <returns>array of double</returns>
    public double[] Darray()
    {
        double[] d = new double[LayerList.Count];
        for (int i = 0; i < LayerList.Count; i++) d[i] = LayerList[i].Thickness;
        return d;
    }

    /// <summary>
    /// get a list of the material abbreviations used on each layer
    /// </summary>
    /// <returns>a list of material abbreviations</returns>
    public string[] Marray()
    {
        string[] mm = new string[LayerList.Count];
        for (int i = 0; i < LayerList.Count; i++) mm[i] = LayerList[i].Material;
        return mm;
    }

    public void ClearCorrections()  //!< clear any thickness corrections
    {
        for (int i = 0; i < LayerList.Count; i++) LayerList[i].Correction = 0;
    }

    public bool HasVariantnk    //!< design has any material set as fitting parameter?
    {
        get
        {
            bool nkv = false;
            foreach (DesignLayer dsl in LayerList)
            {
                nkv |= dsl.nkVariant;
            }
            return nkv;
        }
    }

    /// <summary>
    /// get lists for layers with their n/k models for optimization
    /// </summary>
    /// <returns>lists of index/k models for all layers</returns>
    public IndexModels GetNKVariants()
    {
        var nvary = new IndexModels();
        nvary.material = new List<string>();
        nvary.n_model = new List<int>();
        nvary.k_model = new List<int>();
        nvary.active = new List<bool>();
        foreach (DesignLayer ly in LayerList)
        {
            if (!nvary.material.Contains(ly.Material))
            {
                nvary.material.Add(ly.Material);
                nvary.n_model.Add((int)ly.nModel);
                nvary.k_model.Add((int)ly.kModel);
                nvary.active.Add(ly.nkVariant);
            }
        }
        return nvary;
    }

    public ORE_IF.OptiCorrections MeasCorr
    {
        get; set;
    }

    /// <summary>
    /// send a design to OptiRE
    /// do not overwrite an existing design
    /// </summary>
    /// <param name="comment">optional comment </param>
    /// <returns>success</returns>
    public bool SendToORE(string comment)
    {
        var designs = ORE_IF.DesignList();
        if (designs.Contains(DesignName))
        {
            // design name may exists, but is possibly different
            ORE_IF.DeleteDesign(this.DesignName);
            //return false;
        }
        double[] dnm = this.Darray();
        string[] MatAbbr = this.Marray();
        bool isOK = ORE_IF.LoadThDesign(this.NoLayers, dnm, MatAbbr, DesignName, comment);
        return isOK;
    }

}

/// <summary>
/// class structure for a layer in the design, which can be used as data in Objectview
/// </summary>
[SupportedOSPlatform("Windows7.0")]
public class DesignLayer
{
    public DesignLayer(int ln, double thick, string mat, double tol)
    {
        this.LayNo = ln;
        this.Material = mat;
        this.Tolerance = tol;
        this.Thickness = thick;
        this.Correction = 0;
        this.nModel = NSysModels.nsLoaded;
        this.kModel = KSysModels.ksLoaded;
    }
    public int LayNo { get; set; }  //!< layer position in design substrate(1) to top
    public double Tolerance { get; set; } = 10.0;   //!< thickness tolerance band
    public string Material { get; set; } = "NA";   //!< material abbreviation 
    public NSysModels nModel { get; set; }  //!< index model
    public KSysModels kModel { get; set; }  //!< k-model
    public string nModel_type       //!< get the name of the index model enum
    {
        get => Enum.GetName(typeof(NSysModels), nModel);
        set
        {
            var res = Enum.TryParse<NSysModels>(value, out NSysModels nm);
            nModel = res ? nm : NSysModels.nsLoaded;
        }
    }
    public string kModel_type   //!< get the name of the k-model enum
    {
        get => Enum.GetName(typeof(KSysModels), kModel);
        set
        {
            var res = Enum.TryParse<KSysModels>(value, out KSysModels km);
            kModel = res ? km : KSysModels.ksLoaded;
        }
    }
    public bool nkVariant   //!< is index free for fit?
    {
        get
        {
            return ((nModel != NSysModels.nsLoaded) || (kModel != KSysModels.ksLoaded));
        }
    }

    public double Thickness { get; set; }

    public bool isVar = true;

    private double pctcorr = 0;
    private double pctcorr_org = 0;
    public double CorrectionToBase { get { return pctcorr_org; } }
    public double Correction    //!< thickness correction values based on fit
    {
        get { return pctcorr; }
        set { if (value == 0) avgcnt = 0; pctcorr = value; _cchange = true; }  // reset the average counter if cleared
    }
    private int avgcnt = 0;
    public void AddCorrAvg(double nav)
    {
        ++avgcnt;
        pctcorr += (nav - pctcorr) / (avgcnt);      // calculate a running average
        pctcorr_org = pctcorr;      // copy the result into the save buffer
        _cchange = true;        // set the change flag
    }
    public void ResetCorrection() { pctcorr = pctcorr_org; _cchange = true; }
    private bool _cchange = false;
    public bool CorrChanged { get { return _cchange; } set { _cchange = value; } }
    public DesignLayer Clone()
    {
        DesignLayer cln = new(LayNo, Thickness, Material, Tolerance)
        {
            kModel = kModel,
            nModel = nModel
        };
        return cln;
    }
}

/// <summary>
/// a structure of spectral measurement data
/// </summary>
internal class MeasurementSet
{
    public int points = 0;  //!< number of measurement points
    public double Angle = 0.0;  //!< incidence angle
    public Array Lambda;    //!< wavelength in nm 
    public Array Intensity; //!< intensity %
    public Array Tolerance; //!< tolerance %
    public string MsrmtType = "NA"; //!< type of measurement T/RA/BRA

}
